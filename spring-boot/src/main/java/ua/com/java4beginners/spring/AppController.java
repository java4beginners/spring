package ua.com.java4beginners.spring;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

    @GetMapping("/*")
    public String indexPage() {
        return "Hello from Spring Boot!";
    }
}
