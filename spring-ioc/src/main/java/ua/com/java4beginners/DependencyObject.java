package ua.com.java4beginners;

import org.springframework.stereotype.Component;

@Component
public class DependencyObject {
    public String getMessage() {
        String msg = "This is instance of DependencyObject [%s].";
        return String.format(msg, this.toString());
    }
}
