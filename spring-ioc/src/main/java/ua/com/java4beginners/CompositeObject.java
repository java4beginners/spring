package ua.com.java4beginners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CompositeObject {

    private DependencyObject dependency;

    @Autowired
    public CompositeObject(DependencyObject dependency) {
        this.dependency = dependency;
    }

    public String getMessage() {
        String msg = "This is the instance of CompositeObject [%s]\n\t with injected [%s]\n\t that says '%s'";
        return String.format(msg, this.toString(), dependency.toString(), dependency.getMessage());
    }
}
