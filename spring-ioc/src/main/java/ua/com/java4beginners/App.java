package ua.com.java4beginners;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main( String[] args ) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext("ua.com.java4beginners");
        CompositeObject bean = ctx.getBean(CompositeObject.class);

        System.out.println(bean.getMessage());
    }
}
